$verbosePreference = "Continue"
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process -Force

Import-Module WebAdministration
Import-Module "$(Split-Path -Path $MyInvocation.MyCommand.Definition -Parent)\Powershell.MWA-master\Powershell.MWA.psm1"
Push-Location $env:temp


<#
.SYNOPSIS
    IIS deployment utilities
.DESCRIPTION
    Run deploy powesrshell as administrator. 
     
.EXAMPLE 
    powershell> 
        Import-Module C:\inetpub\sphsc_website_admin\deploy\sphsc-deploy.ps1
    powershell> 
        My-Function
    powershell> 
        sRunAdministatorScripts -doDatabseUpdate $true 

.EXAMPLE
   CreateSite -ComputerName "" -SiteName "spchs-testy" 

.NOTES
#>


$NL = "`r`n=============================== "


function sphsc_update_admin_scripts
{
    [CmdletBinding()]
    Param
    (

        [Parameter(Mandatory=$false)]
        [string]$repoAddress = "https://meyerjn@bitbucket.org/meyerjn/shsc_admin_www.git", 

        [Parameter(Mandatory=$false)]
        [string]$destinationPath = "C:\inetpub\sphsc_website_admin"

    )

    Begin {}

    Process
    {


        "$NL Extracting admin repository into .... $destinationPath"
        _remove_folder -path $destinationPath
        _download_repo -destinationPath $destinationPath -repoAddress $repoAddress

    }

}


function sphsc_import_database
{
    [CmdletBinding()]
    Param
    (

        [Parameter(Mandatory=$false)]
        [bool]$doDatabseUpdate = $false,

        [Parameter(Mandatory=$false)]
        [string]$databaseUpdateScript = "C:\inetpub\sphsc_website_admin\db\database-update.mysql"

    )

    Begin {}

    Process
    {

        if($doDatabseUpdate -eq $true) {

            "$NL Importing script into MySQL ...... $databaseUpdateScript" 
            _configure_db -databaseImportFile $databaseUpdateScript 
        }

    }

}


function sphsc_deploy_website
{
    [CmdletBinding()]
    Param ()

    Begin {}

    Process
    {
        
        _do_deploy


    }

}


function _do_deploy 
{
    [CmdletBinding()]
    Param
    (

        [Parameter(Mandatory=$false)]
        [string]$hostName = "staging.sphsc.washington.edu",
        
        [Parameter(Mandatory=$false)]
        [string]$databaseName = "sphsc_drupal", 

        [Parameter(Mandatory=$false)]
        [string]$repository = "https://meyerjn@bitbucket.org/meyerjn/shscience_dr7v1.git"

    )

    Begin {}

    Process
    {
        try
        {

            function _backupFolders() 
            {

                "$NL Creating web folders ......."
                $archiveDir = (Join-Path $env:SystemDrive "i-archive")
                $dateString = "" + $(Get-Date).Year + $(Get-Date).Month + $(Get-Date).Day  

                _create_folder -path $sitePath
                _create_folder -path $archiveDir
                _prune_folder -folderToPrune $archiveDir

                "$NL Copying to archives ......" 
                _archive_folder -sourcePath $sitePath -destinationPath (Join-Path $archiveDir $($hostName +"-"+ $dateString))
                _archive_db -destinationPath (Join-Path $archiveDir $($hostName +"-"+ $dateString +"-"+ "database"))

                _remove_folder -path $tempPath
                _create_folder -path $tempPath



            }

            function _copyUserFiles() 
            {
            
                $privateFiles = "$(Split-Path $sitePath)\private_for_drupal"
                
                "$NL Creating Drupal's private files folder ......" 
                _create_folder -path $privateFiles 

                "$NL Copying user files ....." 
                Copy-Item -Path (Join-Path $sitePath "\sites\default\files") -Destination (Join-Path $tempPath "\sites\default\") -Recurse -Force

                "$NL Setting upload folder permissions ......" 
                icacls (Join-Path $tempPath "\sites\default\files") /grant Everyone:F /t
                icacls $privateFiles /grant Everyone:F /t

            }
 

            cls

            "$NL $NL RUNNING DEPLOY DEFAULTS ...... $NL" 

            $sitePath = Join-Path $env:SystemDrive (Join-Path "inetpub" $hostName)
            $tempPath = $sitePath +"-temp"

            _backupFolders 

            "$NL Downloading Drupal to temp ...... $tempPath"
            _download_repo -destinationPath $tempPath -repoAddress $repository
            _copyUserFiles 

            "$NL Setting up database ....."
            _configure_db -destinationPath $tempPath

            "$NL Removing current IIS site ...." 
            Remove-Website -Name "$hostName"

            "$NL Adding IIS site (with imported IMA modules) ...."
            New-IisSite -ComputerName localhost -SiteName $hostName -CodePath $sitePath -Bindings "*:80:$($hostName)"
            Get-IisSite -ComputerName localhost -SiteName $hostName
            Stop-IisSite -ComputerName localhost -SiteName $hostName

            "$NL Renaming temp folders ....... $tempPath"
            "      becomes $sitePath ...." 
            _remove_folder -path $sitePath 
            Rename-Item -Path $tempPath -NewName $sitePath 


            "$NL Starting the IIS website ..... "
            Start-IisSite -ComputerName localhost -SiteName $hostName 


 
        }
        catch
        {
            $exceptionMessage = "Error in Line: " + $_.Exception.Line + ". " + $_.Exception.GetType().FullName + ": " + $_.Exception.Message + " Stacktrace: " + $_.Exception.StackTrace

            #Restore-WebConfiguration $backupName

            "$NL $NL Erroring out ...."
            $exceptionMessage
            "$NL Origional message ...."
            throw

        }
    }
}


function _remove_folder
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [string]$path = ""
    )

    Begin {}

    Process
    {
        "$NL Removing $path ....."
        if((Test-Path -Path $path)) { 
            Remove-Item -Path $path -Recurse -Force -Verbose:$true
        }

    }

}


function _create_folder
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [string]$path = ""
    )

    Begin {}

    Process
    {
        
        "$NL Creating $path ....."
        if(!(Test-Path -Path $path)) { 
            New-Item -ItemType Directory -Path $path -Verbose:$true
        }
        Dir $path

    }

}



function _archive_folder
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [string]$sourcePath = "",

        [Parameter(Mandatory=$true)]
        [string]$destinationPath = ""
    )

    Begin {}

    Process
    {

        "$NL Creating folder $destinationPath ..... "
        _remove_folder -path $destinationPath
        _create_folder -path $destinationPath

        
        #"Moving any duplicates ... " 
        #if((Test-Path -Path $destinationPath)) { 
        #    $n = $destinationPath +"."+ (dir -Path $arhivePath -Directory).Count
        #    Rename-Item -Path $destinationPath -NewName $n 
        #}
        
        "$NL Arhive copying $sourcePath ....."
        "       to $destinationPath"
        Copy-Item $sourcePath -Destination $destinationPath -Recurse -Force
        Dir $destinationPath


    }

}


function _archive_db
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$false)]
        [string]$databaseName = "sphsc_drupal",

        [Parameter(Mandatory=$true)]
        [string]$destinationPath = ""
    )

    Begin {}

    Process
    {


        "$NL Creating folder $destinationPath ..... "
        _remove_folder -path $destinationPath
        _create_folder -path $destinationPath

        
        $fileName = "$destinationPath\$databaseName.sql" 

        "$NL Outputting MySQL dump of '$databaseName' to  ..... $fileName"
        mysqldump -u root --databases $databaseName > $fileName
        Dir $destinationPath

        "$NL Creating MySQL hotcopy of database ....."
        $databaseSource = $env:ProgramFiles +"\MySQL\MySQL Server 5.5\data\" + $databaseName

        if(!(Test-Path -Path $databaseSource)) { 
            $databaseSource = mysql -uroot -h'127.0.0.1' -sNe"SELECT @@datadir"
        }

        "$NL Copying $databaseSource  .... to $destinationPath"
        Copy-Item "$databaseSource" -Destination "$destinationPath" -Recurse -Force
        Dir $destinationPath


    }

}


function _prune_folder
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [string]$folderToPrune = "",

        [Parameter(Mandatory=$false)]
        [int]$numberToKeep = 10

    )

    Begin {}

    Process
    {

        "$NL Deleting folders older than first $numberToKeep ...."
        " In the folder $archiveDir"

        $dirs =  dir -Path $folderToPrune -Directory
        "$NL Found $($dirs.Count) archive folders"

        if ($dirs.count -ge $numberToKeep) {
            "Deleting  ...." 
            $oldest = $dirs | Sort-Object CreationTime -Descending | Select-Object -First ($dirs.Count - $numberToKeep)
            $oldest | Format-List
            $oldest | Remove-Item -Recurse -Force
        }

    }

}


function _download_repo
{
    [CmdletBinding()]
    Param
    (

        [Parameter(Mandatory=$false)]
        [string]$repoAddress = "",

        [Parameter(Mandatory=$true)]
        [string]$destinationPath = ""

    )

    Begin {}

    Process
    {

        _create_folder -path $destinationPath

        #Import-Module posh-git 
        #$sshKey = "C:\inetpub\_git_ssh_keys\id_rsa"
        #$env:path += ";${env:ProgramFiles(x86)}\Git\bin"
        #ssh-agent bash -c "ssh-add $sshKey; git clone meyerjn@bitbucket.org:meyerjn/shscience_dr7v1.git
        # 
        # git clone https://meyerjn@bitbucket.org/meyerjn/shsciences_dr7v1.git

        "$NL Downloading clone of repository ...." 
        "git clone $repoAddress $destinationPath"
        git clone $repoAddress $destinationPath
        Dir $destinationPath

        "$NL Trying a repository pull ...." 
        git pull $repoAddress 

    }

}


function _configure_db
{
    [CmdletBinding()]
    Param
    (

        [Parameter(Mandatory=$false)]
        [string]$databaseName = "sphsc_drupal",

        [Parameter(Mandatory=$false)]
        [string]$databaseUser = "sphsc_drupal",

        [Parameter(Mandatory=$false)]
        [string]$databaseImportFile = "", # e.g. "./db/import.sql"

        [Parameter(Mandatory=$false)]
        [string]$destinationPath = ""

    )

    Begin {}

    Process
    {

        function _addDatabaseUser () {

            "$NL Creating no-password database account ...."
            mysql -uroot -h'127.0.0.1' --verbose -e "GRANT ALL ON ``$databaseName``.* TO '$databaseUser'"
            mysql -uroot -h'127.0.0.1' --verbose -e "FLUSH PRIVILEGES"
            mysql -uroot -h'127.0.0.1' --verbose -e "SET PASSWORD FOR '$databaseUser'@'localhost' = PASSWORD('')"
            mysql -uroot -h'127.0.0.1' --verbose -e "FLUSH PRIVILEGES"
            
            #"$NL Testing MySQL access ....."
            #mysql --user="$databaseUser" --database="$databaseName" -e "SHOW TABLES"

        }

        function _addConnectionString () {
            
            $settingsFile = "$destinationPath\sites\default\settings.php" 
            $connectionString = "`$databases['default']['default'] = array (
                'host' => 'localhost', 
                'port' => '',
                'driver' => 'mysql',
                'prefix' => '', 
                'database' => '$databaseName', 
                'username' => '$databaseUser',
                'password' => '', 
                );"
                
            "$NL Adding database connection string to $settingsFile ....." 
            Add-Content -Path "$settingsFile" -Value "`r`n$connectionString" 
            Get-Content -Path "$settingsFile" -Tail 22

        }

        function _importDatabase () {

            "$NL Creating database $databaseName ....." 
            mysql -uroot -h'127.0.0.1' --verbose -e "DROP DATABASE IF EXISTS $databaseName" 
            mysql -uroot -h'127.0.0.1' --verbose -e "CREATE DATABASE ``$databaseName`` CHARACTER SET utf8 COLLATE utf8_general_ci" 
            
            "$NL Importing database into $databaseName from $databaseImportFile ....." 
            mysql -uroot -h'127.0.0.1' $databaseName -e "source $($databaseImportFile)"
            
        }


        if($databaseUser -ne "") {
            _addDatabaseUser
        }

        if($databaseImportFile -ne "") {
            _importDatabase
        }

        if($destinationPath -ne "") {
            _addConnectionString
        }

    }

}



Get-Command -name "sphsc*" 